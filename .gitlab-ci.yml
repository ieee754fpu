image: debian:10

cache:
    when: always
    paths:
        - ccache

build:
    stage: build
    before_script:
        - apt-get update
        # one package per line to simplify sorting, git diff, etc.
        - >-
            apt-get -y install
            autoconf
            bison
            build-essential
            ccache
            clang
            cmake
            curl
            flex
            gawk
            git
            gperf
            libboost-program-options-dev
            libffi-dev
            libftdi-dev
            libgmp-dev
            libmpfr-dev
            libreadline-dev
            mercurial
            pkg-config
            python
            python3
            python3-dev
            python3-keyring
            python3-pip
            python3-setuptools
            python3-venv
            python3-wheel
            tcl-dev
        - export PATH="/usr/lib/ccache:$PATH"
        - export CCACHE_BASEDIR="$PWD"
        - export CCACHE_DIR="$PWD/ccache"
        - export CCACHE_COMPILERCHECK=content
        - ccache --zero-stats || true
        - ccache --show-stats || true
        - python3 -m venv --system-site-packages .venv
        - . .venv/bin/activate
        - pip install pytest-xdist==3.3.1 pytest==7.3.1
    script:
        - . .venv/bin/activate

        - IEEE754FPU_PATH="$(pwd)"
        - git clone --depth 1 --recursive -b v0.6.0 https://github.com/billzorn/sfpy.git sfpy
        - pushd sfpy
        - git apply "$IEEE754FPU_PATH"/sfpy.patch
        - pushd berkeley-softfloat-3
        - git apply "$IEEE754FPU_PATH"/berkeley-softfloat.patch
        - popd
        - pushd SoftPosit
        - git apply ../softposit_sfpy_build.patch
        - git apply "$IEEE754FPU_PATH"/SoftPosit.patch
        - popd
        - pip install -r requirements.txt
        - make lib -j$(nproc)
        - make cython -j$(nproc)
        - make wheel -j$(nproc)
        - pip install --force-reinstall dist/sfpy*.whl
        - popd

        - git clone --depth 1 -b smtlib2-expr-support-on-0.13 https://git.libre-soc.org/git/yosys.git yosys
        - pushd yosys
        - git rev-parse HEAD
        - make config-gcc
        - make -j$(nproc)
        - make install
        - popd
        - yosys -V

        - git clone https://git.libre-soc.org/git/SymbiYosys.git sby
        - pushd sby
        - git checkout db740839b737ee55b8b39f1b29780872d32d248a
        - make install
        - popd

        - git clone --depth 1 -b Yices-2.6.4 https://github.com/SRI-CSL/yices2.git yices2
        - pushd yices2
        - autoconf
        - ./configure
        - make -j$(nproc)
        - make install
        - popd

        - git clone --depth 1 -b z3-4.8.17 https://github.com/Z3Prover/z3.git z3
        - pushd z3
        - python scripts/mk_make.py
        - cd build
        - make -j$(nproc)
        - make install
        - popd

        - git clone https://github.com/bitwuzla/bitwuzla.git bitwuzla
        - pushd bitwuzla
        - git checkout 19dd987a6e246990619751cca07996fac505fd0b
        - ./contrib/setup-btor2tools.sh
        - ./contrib/setup-symfpu.sh
        - ./contrib/setup-cadical.sh
        - ./configure.sh
        - cd build
        - make -j$(nproc)
        - make install
        - popd

        - git clone --depth 1 https://gitlab.com/nmigen/nmigen.git nmigen
        - pushd nmigen
        - git rev-parse HEAD
        - python3 setup.py develop
        - popd

        - git clone --depth 1 https://git.libre-soc.org/git/nmutil.git nmutil
        - pushd nmutil
        - git rev-parse HEAD
        - python3 setup.py develop
        - popd

        - git clone --depth 1 https://git.libre-soc.org/git/pytest-output-to-files.git pytest-output-to-files
        - pushd pytest-output-to-files
        - git rev-parse HEAD
        - python3 setup.py develop
        - popd

        - python3 setup.py develop

        - pytest -v
